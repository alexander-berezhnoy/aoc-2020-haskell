{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Control.Exception (IOException, catch, try)
import Control.Monad (zipWithM_)
import qualified Day01
import qualified Day02
import qualified Day03
import qualified Day04
import qualified Day05
import qualified Day06
import qualified Day07
import qualified Day08
import qualified Day09
import qualified Day10
import qualified Day14
import qualified Day15
import qualified Day16
import qualified Day18
import Debug.Trace (traceIO)
import GHC.Clock (getMonotonicTime)
import System.Environment (getArgs)
import System.FilePath.Posix ((</>))
import Text.Printf (printf)

skip _ = (0, 0)

days =
  [ Day01.solve,
    Day02.solve,
    Day03.solve,
    Day04.solve,
    Day05.solve,
    Day06.solve,
    Day07.solve,
    Day08.solve,
    Day09.solve,
    Day10.solve,
    skip,
    skip,
    skip,
    Day14.solve,
    Day15.solve,
    Day16.solve,
    skip,
    Day18.solve
  ]

runDay :: Int -> (String -> (Int, Int)) -> IO ()
runDay num solve = do
  printf "\nSolving day %d\n" num
  let filename = "input" </> printf "day%02d.txt" num
  content :: Either IOException String <- try $ readFile filename
  case content of
    Left ex -> printf "%s, skipping day %d\n" (show ex) num
    Right input -> do
      start <- getMonotonicTime
      let (p1, p2) = solve input
      printf "Part 1: %d\n" p1
      printf "Part 2: %d\n" p2
      end <- getMonotonicTime
      printf "Time spent: %f seconds\n" (end - start)

runAll = zipWithM_ runDay [1 ..] days

runSome ns = zipWithM_ runDay ns (map (\n -> days !! (n - 1)) ns)

main :: IO ()
main = do
  args <- getArgs
  if null args then runAll else runSome $ map read args
