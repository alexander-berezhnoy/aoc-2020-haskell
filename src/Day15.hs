{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

-- Rambunctious Recitation: https://adventofcode.com/2020/day/15
module Day15 where

import Control.Monad.ST (ST, runST)
import qualified Data.HashTable.ST.Basic as HT
import qualified Data.IntMap as M
import Data.List.Split (splitOn)
import Data.Maybe (fromMaybe, maybe)
import Data.STRef (newSTRef, readSTRef, writeSTRef)

play :: [Int] -> Int -> Int
play (x : xs) turns = runST $ do
  h <- HT.newSized turns
  play' xs 1 x h
  where
    play' seed turn last h
      | turn == turns = return last
      | otherwise = do
        lastTurn <- HT.lookup h last
        let (last', seed') = case seed of
              (s : ss) -> (s, ss)
              _ -> (maybe 0 (turn -) lastTurn, [])
        HT.insert h last turn
        play' seed' (turn + 1) last' h

solve input =
  let seed = map read $ splitOn "," input
   in (play seed 2020, play seed 30000000)