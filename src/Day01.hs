-- Report Repair: https://adventofcode.com/2020/day/1
module Day01 where

import Data.List (sort)
import Data.Maybe (fromJust)

parseInput :: String -> [Int]
parseInput s = map read $ lines s

find :: Int -> Int -> [Int] -> Maybe [Int]
find _ _ [] = Nothing
find target count (x : xs)
  | count == 0 = Nothing
  | x == target && count == 1 = Just [x]
  | x < target = case find (target - x) (count - 1) xs of
    Just found -> Just (x : found)
    _ -> find target count xs
  | otherwise = Nothing

solve' count = product . fromJust . find 2020 count

solve :: String -> (Int, Int)
solve input =
  let d = sort $ parseInput input
   in (solve' 2 d, solve' 3 d)