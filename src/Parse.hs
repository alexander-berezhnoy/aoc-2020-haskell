module Parse where

import Data.Sequence (viewr)
import qualified Text.Parsec as P
import Text.Parsec.String (Parser)

parse :: Parser v -> String -> v
parse p s = either (error . show) id $ P.parse p "<unknown>" s

parseMaybe :: Parser v -> String -> Maybe v
parseMaybe p s = either (const Nothing) Just $ P.parse p "<unknown>" s