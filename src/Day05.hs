-- Binary Boarding: https://adventofcode.com/2020/day/5
module Day05 where

import Data.Bits (shift)
import Data.List (sort)

parseTicket :: String -> Int
parseTicket s = p' s 0
  where
    p' [] v = v
    p' (x : xs) v =
      let v' = shift v 1 + fromEnum (x == 'B' || x == 'R')
       in p' xs v'

parseInput = map parseTicket . lines

findSpot (x : y : xs) | y == x + 2 = x + 1
findSpot (_ : xs) = findSpot xs
findSpot _ = error "must not happen"

solve input =
  let tickets = sort $ parseInput input
   in (last tickets, findSpot tickets)
