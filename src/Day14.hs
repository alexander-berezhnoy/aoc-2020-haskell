-- Docking Data: https://adventofcode.com/2020/day/14
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Day14 where

import Control.Monad.ST (runST)
import Data.Bits (shift, (.&.), (.|.))
import qualified Data.Map as M
import Parse (parse)
import Text.Parsec (char, digit, many1, sepBy1, space, string, try, (<|>))
import Text.Parsec.String (Parser)

data Command = Mask String | Mem Int Int

type Memory = M.Map Int Int

type MemoryUpdate = Int -> Int -> Memory -> Memory

maskToUpdate1 :: String -> MemoryUpdate
maskToUpdate1 = maskToUpdate' 0 0
  where
    maskToUpdate' or and [] = \addr value -> M.insert addr (value .&. and .|. or)
    maskToUpdate' or and (x : xs) =
      let (orBit, andBit) = case x of
            'X' -> (0, 1)
            '1' -> (1, 1)
            '0' -> (0, 0)
       in maskToUpdate' (or `shift` 1 + orBit) (and `shift` 1 + andBit) xs

maskToUpdate2 :: String -> MemoryUpdate
maskToUpdate2 = maskToUpdate' (const [0])
  where
    maskToUpdate' fn [] =
      let insertAll = foldl (\m (k, v) -> M.insert k v m)
          update addr value mem = insertAll mem [(a, value) | a <- fn addr]
       in update
    maskToUpdate' fn (x : xs) =
      let bitFn = case x of
            'X' -> const [0, 1]
            '1' -> const [1]
            '0' -> return . (.&. 1)
          newFn x = [g + (h `shift` 1) | g <- bitFn x, h <- fn $ x `shift` (-1)]
       in maskToUpdate' newFn xs

mask :: Parser Command
mask = do
  string "mask" >> space >> char '=' >> space
  Mask <$> many1 (char 'X' <|> char '1' <|> char '0')

num = read <$> many1 digit

mem :: Parser Command
mem = do
  string "mem["
  addr <- num
  char ']' >> space >> char '=' >> space
  Mem addr <$> num

parseInput = parse (sepBy1 (try mask <|> mem) (char '\n'))

solveGeneric :: (String -> MemoryUpdate) -> [Command] -> Int
solveGeneric maskToUpd = solve' (const (const id)) M.empty
  where
    solve' _ mem (Mask m : xs) = solve' (maskToUpd m) mem xs
    solve' updFn mem (Mem addr val : xs) =
      let mem' = updFn addr val mem
       in solve' updFn mem' xs
    solve' _ mem [] = sum $ map snd $ M.toList mem

solve1 :: [Command] -> Int
solve1 = solveGeneric maskToUpdate1

solve2 :: [Command] -> Int
solve2 = solveGeneric maskToUpdate2

solve input =
  let program = parseInput input
   in (solve1 program, solve2 program)