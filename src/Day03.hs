-- Toboggan Trajectory: https://adventofcode.com/2020/day/3
module Day03 where

data State = State {width :: Int, pos :: Int, forrest :: [[Int]]}

type Slope = (Int, Int)

move :: State -> Slope -> State
move (State w pos f) (r, d) =
  let newPos = (pos + r) `mod` w
      newForrest = drop d f
   in State w newPos newForrest

parseInput :: String -> State
parseInput s =
  let forrest = [map (fromEnum . ('#' ==)) line | line <- lines s]
   in State (length $ head forrest) 0 forrest

countTrees :: State -> Slope -> Int
countTrees State {forrest = []} slope = 0
countTrees st@State {pos = pos, forrest = (x : _)} slope =
  x !! pos + countTrees (move st slope) slope

solve input =
  let state = parseInput input
      sol1 = countTrees state (3, 1)
      steps2 = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
      sol2 = product $ map (countTrees state) steps2
   in (sol1, sol2)
