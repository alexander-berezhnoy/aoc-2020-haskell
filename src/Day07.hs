-- Handy Haversacks: https://adventofcode.com/2020/day/7
module Day07 where

import Data.Functor ((<&>))
import Data.Map ((!?))
import qualified Data.Map as M
import Data.Maybe (fromMaybe)
import qualified Data.Set as S
import Data.Tuple (swap)
import Parse (parse)
import Text.Parsec (char, digit, letter, many1, optional, sepBy1, space, string, (<|>))
import Text.Parsec.String (Parser)

type Bag = String

type BagTree = M.Map Bag [(Int, Bag)]

word :: Parser String
word = many1 letter

num :: Parser Int
num = many1 digit <&> read

bag = do
  color1 <- word
  space
  color2 <- word
  space
  string "bag"
  optional $ char 's'
  return $ color1 ++ " " ++ color2

bagAmount = do
  n <- num
  space
  b <- bag
  return (n, b)

bagList = sepBy1 bagAmount (string ", ") <|> noOtherBags

noOtherBags :: Parser [(Int, Bag)]
noOtherBags = string "no other bags" >> return []

sentence :: Parser (Bag, [(Int, Bag)])
sentence = do
  b <- bag
  space
  string "contain"
  space
  bs <- bagList
  char '.'
  return (b, bs)

parseInput :: String -> BagTree
parseInput = M.fromList . map (parse sentence) . lines

invertBagTree :: BagTree -> M.Map Bag [Bag]
invertBagTree t =
  let invertPair (k, vs) = [(v, [k]) | (_, v) <- vs]
      pairs = concatMap invertPair $ M.toList t
   in M.fromListWith (++) pairs

solve1 :: BagTree -> Int
solve1 t =
  let t' = invertBagTree t
      get = fromMaybe [] . (t' !?)
      solve' s [] = s
      solve' s (b : q) =
        let seen = S.member b s
            (s', q') = if seen then (s, q) else (S.insert b s, q ++ get b)
         in solve' s' q'
   in S.size (solve' S.empty ["shiny gold"]) - 1

solve2 :: BagTree -> Int
solve2 t = solve' 1 "shiny gold" - 1
  where
    solve' n bag =
      let content = fromMaybe [] (t !? bag)
          sub (n', b) = solve' (n' * n) b
       in n + sum (map sub content)

solve input =
  let bags = parseInput input
   in (solve1 bags, solve2 bags)
