-- Custom Customs: https://adventofcode.com/2020/day/6
module Day06 where

import qualified Data.Set as S

type Line = S.Set Char

type Group = [Line]

type Operation = Line -> Line -> Line

parseInput :: String -> [Group]
parseInput s = parse' (lines s) [] []
  where
    parse' [] groups g = g : groups
    parse' ("" : xs) groups g = parse' xs (g : groups) []
    parse' (x : xs) groups g = parse' xs groups (S.fromList x : g)

evalGroup :: Operation -> Group -> Int
evalGroup op group = S.size $ foldl1 op group

solve' :: Operation -> [Group] -> Int
solve' op = sum . map (evalGroup op)

solve input =
  let groups = parseInput input
   in (solve' S.union groups, solve' S.intersection groups)
