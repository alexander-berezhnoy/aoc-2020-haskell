module Day18 where

import Parse (parse)
import Text.Parsec (digit, many1, (<|>))
import Text.Parsec.Expr (Assoc (AssocLeft), Operator (Infix), buildExpressionParser)
import Text.Parsec.String (Parser)
import Text.Parsec.Token (natural, parens, reservedOp)
import Text.ParserCombinators.Parsec.Language (javaStyle)
import Text.ParserCombinators.Parsec.Token (makeTokenParser)

lexer = makeTokenParser javaStyle

buildParser table =
  let expr = buildExpressionParser table term
      term = parens lexer expr <|> natural lexer
   in expr

mul = Infix (reservedOp lexer "*" >> return (*)) AssocLeft

add = Infix (reservedOp lexer "+" >> return (+)) AssocLeft

table1 = [[add, mul]]

table2 = [[add], [mul]]

solve' table exprs =
  let parser = buildParser table
   in fromInteger $ sum $ map (parse parser) exprs

solve input =
  let exprs = lines input
   in (solve' table1 exprs, solve' table2 exprs)