-- Handheld Halting: https://adventofcode.com/2020/day/8
module Day08 where

import Control.Monad (ap)
import Data.Char (toUpper)
import qualified Data.Set as S
import Data.Vector ((!), (//))
import qualified Data.Vector as V
import Parse (parse)
import Text.Parsec (char, count, digit, letter, many1, sepBy1, space, string, (<|>))
import Text.Parsec.String (Parser)

data Instruction = Nop Int | Acc Int | Jmp Int deriving (Eq, Read)

sign :: Num a => Parser (a -> a)
sign = (char '-' >> return negate) <|> (char '+' >> return id)

nat :: Parser Int
nat = read <$> many1 digit

int = sign `ap` nat

instruction :: Parser Instruction
instruction = do
  c <- count 3 letter
  space
  arg <- int
  return $ case c of
    "nop" -> Nop arg
    "acc" -> Acc arg
    "jmp" -> Jmp arg
    _ -> error ""

data Machine = Machine
  { code :: V.Vector Instruction,
    history :: S.Set Int,
    acc :: Int,
    ptr :: Int
  }

parseInput :: String -> Machine
parseInput s =
  let code = parse (sepBy1 instruction $ char '\n') s
   in Machine (V.fromList code) S.empty 0 0

halts Machine {history = h, ptr = p} = S.member p h

terminates Machine {code = c, ptr = p} = p >= V.length c

fixInstruction :: Bool -> Instruction -> Instruction
fixInstruction True (Nop arg) = Jmp arg
fixInstruction True (Jmp arg) = Nop arg
fixInstruction _ i = i

runStep :: Int -> Machine -> Machine
runStep fix m@Machine {code = c, acc = ac, ptr = p, history = h} =
  let (p', ac') = case fixInstruction (fix == p) (c ! p) of
        Nop arg -> (p + 1, ac)
        Acc arg -> (p + 1, ac + arg)
        Jmp arg -> (p + arg, ac)
   in m {ptr = p', acc = ac', history = S.insert p h}

run :: Machine -> Int -> (Int, Bool)
run m@Machine {acc = a} fix = case (halts m, terminates m) of
  (True, _) -> (a, False) -- halts
  (_, True) -> (a, True) -- terminates
  _ -> run (runStep fix m) fix

solve1 m = fst $ run m (-1)

solve2 m =
  let attempts = map (run m) [0 ..]
      solutions = [a | (a, r) <- attempts, r]
   in head solutions

solve input = let machine = parseInput input in (solve1 machine, solve2 machine)