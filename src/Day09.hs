-- Encoding Error: https://adventofcode.com/2020/day/9
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Day09 where

findSum :: [Int] -> Int -> Bool
findSum = find' False
  where
    find' True (n : ns) x
      | x == n = True
      | otherwise = find' True ns x
    find' False (n : ns) x
      | n < x = find' True ns (x - n) || find' False ns x
      | otherwise = find' False ns x
    find' _ [] _ = False

solve1 as =
  let solve' (hss@(h : hs), tss@(t : ts)) =
        if findSum hss t then solve' (hs ++ [t], ts) else t
   in solve' $ splitAt 25 as

solve2 nums target = solve' 0 [] nums
  where
    solve' sm wss@(w : ws) hss@(h : hs)
      | sm + h == target = answer $ h : wss
      | sm + h < target = solve' (sm + h) (wss ++ [h]) hs
      | otherwise = solve' (sm - w) ws hss
    solve' _ [] (h : hs) = solve' h [h] hs
    answer ns = minimum ns + maximum ns

solve input =
  let nums = map read $ lines input
      sol1 = solve1 nums
   in (sol1, solve2 nums sol1)