module Day04 where

import Data.Functor ((<&>))
import Data.Map ((!?))
import qualified Data.Map as M
import Data.Maybe (fromJust, fromMaybe)
import qualified Data.Set as S
import Debug.Trace (trace)
import Parse (parse, parseMaybe)
import Text.Parsec (anyChar, char, count, digit, eof, hexDigit, letter, many1, sepBy1, sepEndBy1, string, (<|>))
import Text.Parsec.String (Parser)
import Text.Read (readMaybe)

type Field = (String, String)

type Record = M.Map String String

field :: Parser Field
field = do
  name <- count 3 letter
  char ':'
  value <- many1 (letter <|> digit <|> char '#')
  return (name, value)

eol = char '\n'

record = do
  fields <- sepEndBy1 field (char ' ' <|> eol)
  return $ M.fromList fields

parseInput = parse (sepBy1 record eol)

validateRecord1 :: Record -> Bool
validateRecord1 rec = all (`M.member` rec) requiredFields

between a b v = a <= v && v <= b

ensure = fromMaybe False

validateByr s = ensure $ readMaybe s <&> between 1920 2002

validateIyr s = ensure $ readMaybe s <&> between 2010 2020

validateEyr s = ensure $ readMaybe s <&> between 2020 2030

hgt :: Parser (Int, String)
hgt = do
  n <- many1 digit
  unit <- string "cm" <|> string "in"
  return (read n, unit)

validateHgt s = ensure $ do
  (h, unit) <- parseMaybe hgt s
  return $ if unit == "cm" then between 150 193 h else between 59 76 h

validateHcl s = ensure $ do
  parseMaybe (char '#' >> count 6 hexDigit >> eof) s
  return True

eyeColors = S.fromList ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

validateEcl = (`S.member` eyeColors)

validatePid s = ensure $ do
  parseMaybe (count 9 digit >> eof) s
  return True

requiredFields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

validators = [validateByr, validateIyr, validateEyr, validateHgt, validateHcl, validateEcl, validatePid]

validateField rec (name, validator) =
  maybe False validator (rec !? name)

validateRecord2 rec = all (validateField rec) $ zip requiredFields validators

solve input =
  let records = parseInput input
      solve' v = length $ filter v records
   in (solve' validateRecord1, solve' validateRecord2)