module Day16 where

import Data.List (find, isPrefixOf, sortOn)
import qualified Data.Map as M
import Data.Maybe (fromJust)
import qualified Data.Set as S
import Parse (parse)
import Text.Parsec (char, digit, letter, many1, sepBy1, sepEndBy1, space, string, (<|>))
import Text.Parsec.String (Parser)

type Ticket = [Int]

type Constraint = (String, Int -> Bool)

num = read <$> many1 digit

range :: Parser (Int, Int)
range = do
  a <- num
  char '-'
  b <- num
  return (a, b)

phrase :: Parser String
phrase = many1 (letter <|> char ' ')

eol = char '\n'

constraint :: Parser (String, Int -> Bool)
constraint = do
  field <- phrase
  char ':' >> space
  (a1, b1) <- range
  space >> string "or" >> space
  (a2, b2) <- range
  return (field, \x -> (a1 <= x && x <= b1) || (a2 <= x && x <= b2))

ticket :: Parser Ticket
ticket = sepBy1 num $ char ','

inputParser :: Parser ([Constraint], Ticket, [Ticket])
inputParser = do
  cs <- sepEndBy1 constraint eol
  eol >> string "your ticket:" >> eol
  mt <- ticket
  eol >> eol
  string "nearby tickets:" >> eol
  nt <- sepBy1 ticket eol
  return (cs, mt, nt)

simpleVerify :: [Constraint] -> Ticket -> Int
simpleVerify cs t = sum $ filter badField t
  where
    badField v = all (($v) . (not .) . snd) cs

solve1 :: [Constraint] -> [Ticket] -> Int
solve1 cs = sum . map (simpleVerify cs)

filterConstraints :: Int -> [Constraint] -> [Constraint]
filterConstraints v = filter (($v) . snd)

applyTicket :: Ticket -> [[Constraint]] -> [[Constraint]]
applyTicket = zipWith filterConstraints

resolve :: Ord a => [[a]] -> [a]
resolve groups =
  let indexed = zip groups [0 ..]
      sorted = sortOn (length . fst) indexed
      notMember set = not . (`S.member` set)

      resolve' found ((cs, idx) : css) =
        let newFound = fromJust $ find (notMember found) cs
         in (newFound, idx) : resolve' (S.insert newFound found) css
      resolve' _ [] = []
      resolved = resolve' S.empty sorted
   in map fst $ sortOn snd resolved

solve2 :: [Constraint] -> Ticket -> [Ticket] -> Int
solve2 cs t ts =
  let goodTickets = filter ((== 0) . simpleVerify cs) ts
      mapping = replicate (length t) cs
      filtered = foldr applyTicket mapping goodTickets
      filteredNamesOnly = map (map fst) filtered
      resolved = resolve filteredNamesOnly
   in product $ [v | (v, f) <- zip t resolved, "departure" `isPrefixOf` f]

solve input =
  let (cs, mt, ot) = parse inputParser input
   in (solve1 cs ot, solve2 cs mt ot)