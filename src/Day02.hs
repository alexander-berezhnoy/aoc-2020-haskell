-- Password Philosophy: https://adventofcode.com/2020/day/2
module Day02 where

import Data.Either (fromRight)
import Data.Functor ((<&>))
import Data.Maybe (fromJust)
import Parse (parse)
import Text.Parsec (anyChar, char, digit, many1, spaces)
import Text.Parsec.String (Parser)

type Rule = (Int, Int, Char, String)

num :: Parser Int
num = many1 digit <&> read

rule :: Parser Rule
rule = do
  a <- num
  char '-'
  b <- num
  spaces
  c <- anyChar
  char ':'
  spaces
  s <- many1 anyChar
  return (a, b, c, s)

parseInput = map (parse rule) . lines

count :: (a -> Bool) -> [a] -> Int
count f = length . filter f

checkRule1 :: Rule -> Bool
checkRule1 (a, b, c, str) =
  let v = count (c ==) str
   in a <= v && v <= b

checkRule2 :: Rule -> Bool
checkRule2 (a, b, c, str) =
  let get x = str !! (x -1) in (get a == c) /= (get b == c)

solve input =
  let rules = parseInput input
   in (count checkRule1 rules, count checkRule2 rules)
