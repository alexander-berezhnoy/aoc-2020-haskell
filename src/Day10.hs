module Day10 where

import Data.List (sort)
import qualified Data.Map as M
import Debug.Trace (trace)

parseInput :: String -> [Int]
parseInput s = (diff . sort . map read . lines) s ++ [3]

diff a = zipWith (-) a (0 : a)

frequency :: Ord a => [a] -> M.Map a Int
frequency xs = M.fromListWith (+) [(a, 1) | a <- xs]

solve1 = product . map snd . M.toList . frequency

solve input = let diffs = parseInput input in (solve1 diffs, 0)